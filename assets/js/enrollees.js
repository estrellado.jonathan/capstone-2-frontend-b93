console.log(window.location.search);

let params = new URLSearchParams(window.location.search);
let adminUser = localStorage.getItem("isAdmin");
console.log(params.has('courseId'));
console.log(params.get('courseId'));
let courseId = params.get('courseId');
let token = localStorage.getItem('token');
let enrolleesContainer = document.querySelector("#enrolleesContainer");

if (adminUser == "false"  || !adminUser) {
	alert("You don't have administrator rights to this page!")
	window.location.replace("./courses.html")

} else {

	fetch(`https://intense-hamlet-33076.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		console.log(data)

			enrolleesContainer.innerHTML = 
			`
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<h3 class="text-center">${data.name}</h3>
					<h3 class="text-center mt-5">List of Enrollees</h3>
					<table class="table">
						<thead>
							<tr>
								<th> First Name </th>
								<th> Last name </th>
								<th> Enrolled On </th>
							</tr>
						</thead>
						<tbody id="listEnrollees">
						</tbody>
					</table>
				</section>
			</div>
			`
			let users = document.querySelector("#listEnrollees");

			data.enrollees.map(enrollData =>{

				console.log(enrollData);

				fetch(`https://intense-hamlet-33076.herokuapp.com/api/users/${enrollData.userId}`, {
					method: 'GET',
					headers: {
						'Content-Type': 'application/json'
											}
				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					users.innerHTML +=
					`
					<tr>
						<td>${data.firstName}</td>
						<td>${data.lastName}</td>
						<td>${enrollData.enrolledOn}</td>
					</tr>
					`
				})
			})
	})
}