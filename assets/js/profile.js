let token = localStorage.getItem("token");

let profileContainer = document.querySelector("#profileContainer");


if(!token || token === null){

	alert('You must login first.');
	window.location.replace("./login.html");

} else {

	fetch('https://intense-hamlet-33076.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		profileContainer.innerHTML =
		`
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<h3 class="text-center">Name: ${data.firstName} ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="text-center">Mobile Number: ${data.mobileNo}</h3>
					<h3 class="text-center mt-5">Class History</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Course Name</th>
								<th>Enrolled On</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody id="courses">
						</tbody>
					</table>
				</section>
			</div>
		`

		let courses = document.querySelector("#courses");

		data.enrollments.forEach(courseData => {

			fetch(`https://intense-hamlet-33076.herokuapp.com/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data.name);

				courses.innerHTML += 
					`
					<tr>
						<td>${data.name}</td>
						<td>${courseData.enrolledOn}</td>
						<td>${courseData.status}</td>
					</tr>
					`

			})

		})

	})

}